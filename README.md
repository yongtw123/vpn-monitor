# VPN-Monitor

Monitors IPSec/L2TP VPN by actually connecting to it.

## Environment
CentOS 6/7

## Setup
1. Install necessary services: strongswan, xl2tpd
2. Configure IPSec: `/etc/strongswan/ipsec.conf`

```
conn %default
  ikelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev1
  authby=secret
  ike=aes128-sha1-modp1024,3des-sha1-modp1024!
  esp=aes128-sha1-modp1024,3des-sha1-modp1024!

conn <CONN_NAME1>
  keyexchange=ikev1
  authby=secret
  auto=add
  left=%defaultroute
  leftprotoport=17/1701
  right=<VPN_IP>
  rightprotoport=17/1701
```

3. Insert IPSec PSK: `/etc/strongswan/ipsec.secrets`

```
%any <VPN_IP> : PSK "<VPN_PSK>"
```

4. Configure strongSwan: reduce retransmission timeout

```
#...
charon {
	#...
        retransmit_tries = 2
}
#...
```

5. Test IPSec connection. Final line should be like "successfully connected".

```
service strongswan start
strongswan up <CONN_NAME1>
```

6. Setup xl2tpd: `/etc/xl2tpd/xl2tpd.conf`

```
[lac <CONN_NAME2>]
lns = <VPN_IP>
ppp debug = yes
pppoptfile = /etc/ppp/options.xl2tpd.client
length bit = yes
```

7. Setup ppp: `/etc/ppp/options.xl2tpd.client`

```
ipcp-accept-local
ipcp-accept-remote
refuse-eap
require-mschap-v2
noccp
noauth
mtu 1280
mru 1280
noipdefault
defaultroute
debug
lock
connect-delay 5000
name <L2TP login name>
password <L2TP login passwd>
```

8. Test L2TP; connect via IPSec first.

```
service strongswan start
strongswan up <CONN_NAME1>
service xl2tpd start
xl2tpd-control connect <CONN_NAME2>
# check /var/log/messages for connection success message 
xl2tpd-control disconnect <CONN_NAME2>
service xl2tpd stop
strongswan down <CONN_NAME1>
service strongswan stop
```

9. Test run script
10. Setup cron
