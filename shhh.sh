#!/bin/bash
# Monitor L2TP/IPSEC VPN connectivity by trying out
#
# NOTE:
# 1. Sync with strongswan/xl2tpd/ppp configs
# 2. Ensure above service logging on
# 3. Assert log location (default: /var/log/messages)
# 4. Assert log format, especially dates
# 5. [!!!] CHANGE CONN_NAMEs if necessary!!!
# 6. [!] Adverse scenarios not thoroughly tested yet
#
# Author: yongtw123@gmail.com

IPSEC_CONN_NAME="ipseconn"
L2TP_CONN_NAME="l2tpconn"
VPN_FLAG_LOC="/tmp/VPN-deadflag"
ALERT_TARGETS_LIST="$(dirname "$0")/alert_targets.dat"
MAX_ALERT_1=4

waitFor() {
  for ms in 0.2 0.5 1 2 4 8
  do 
    if eval "$1"; then
      return 0
    else
      sleep $ms
    fi
  done
  return 1
}

postmsg() {
  if [[ -f $ALERT_TARGETS_LIST ]]; then
    while read -r ln; do 
      curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"$1\"}" "$ln"
    done < $ALERT_TARGETS_LIST
  fi
}

send_alert() {
  if [[ $1 -eq 0 && -f $VPN_FLAG_LOC ]]; then
    postmsg "$2"
  elif [[ $1 -eq 1 ]]; then
    if [[ -f $VPN_FLAG_LOC && $(cat $VPN_FLAG_LOC) -gt $MAX_ALERT_1 ]]; then
      return
    fi
    postmsg "$2"
  fi
}

set_flag() {
  if [[ $1 -eq 0 && -f $VPN_FLAG_LOC ]]; then
    rm -f $VPN_FLAG_LOC
  elif [[ $1 -eq 1 ]]; then
    touch $VPN_FLAG_LOC
    echo $(( $(cat $VPN_FLAG_LOC) + 1 )) > $VPN_FLAG_LOC
  fi
}

ipsec_cleanup() {
  strongswan down $IPSEC_CONN_NAME > /dev/null
  service strongswan stop > /dev/null 2>&1
  set_flag $1
  exit $1
}

xl2tp_cleanup() {
  xl2tpd-control disconnect $L2TP_CONN_NAME
  service xl2tpd stop > /dev/null 2>&1
  ipsec_cleanup $1
}

service strongswan restart > /dev/null 2>&1
if waitFor "tail -n50 /var/log/messages | grep -qE \"$(date '+%H:%M').*added configuration '$IPSEC_CONN_NAME'\""; then
  if strongswan up $IPSEC_CONN_NAME | grep -q "connection '$IPSEC_CONN_NAME' established successfully"; then
    :  #no-op
  else
    send_alert 1 "IPSEC connection FAILED!"
    ipsec_cleanup 1
  fi
else
  send_alert 1 "Strongswan service FAILED to start. Abort!"
  ipsec_cleanup 1
fi

service xl2tpd start > /dev/null 2>&1
if waitFor "[[ -p /var/run/xl2tpd/l2tp-control ]]"  #check for named pipe
then
  xl2tpd-control connect $L2TP_CONN_NAME  #this command has no stdout
  if waitFor "ip addr show ppp0 2>&1 | grep -qoE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b'"; then
    :  #no-op
  else
    send_alert 1 "L2TP tunneling or PPP interface FAILED!"
    xl2tp_cleanup 1
  fi
else
  send_alert 1 "L2TP service FAILED to start. Abort!"
  xl2tp_cleanup 1
fi

send_alert 0 "VPN connection is UP."
xl2tp_cleanup 0  #no error so far, successful
